package com.tsystems.javaschool.tasks.subsequence;

import java.lang.IllegalArgumentException;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.*;

public class Subsequence {


    public static boolean find(List x, List y) {
        boolean flag=true;
        if(y == null)
        {
            throw new IllegalArgumentException();
        }
        int k=0;
        try{  for (int i=0;i<x.size();i++)
        {
            if (!flag) {break;}
            flag=false;
            for (int j=k;j<y.size();j++)
            {
                if (y.get(j).equals(x.get(i)))
                {
                    k=j+1;
                    flag=true;
                    break;

                }
            }
        }}
        catch (Exception e){
            throw new IllegalArgumentException();
        }

        return flag;

    }
    public static void main(String[] args){
        boolean b = find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b);
    }
}

